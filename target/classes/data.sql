INSERT INTO users (id,active,email,username,password,rating, invite_count)
VALUES (1,true,'mika@reifenberger.com','m1k4','$2a$04$T7JZz7LeyD6dsT4nyy7KhOkpViAHBu6Vdi2eLf1QrZPWysrtgKLIW',1,2);


INSERT INTO role (id, description, name) VALUES (1, 'Admin role', 'ADMIN');
INSERT INTO role (id, description, name) VALUES (2, 'User role', 'USER');

INSERT INTO user_roles (user_id,role_id) VALUES (1,1);
INSERT INTO user_roles(user_id, role_id) VALUES (1,2);

INSERT INTO categories (id, name) VALUES (10, 'sfw');
INSERT INTO categories (id, name) VALUES (11, 'nsfw');
INSERT INTO categories (id, name) VALUES (12, 'nswfl');

INSERT INTO codes (id, code) VALUES (13, 'secretCode');