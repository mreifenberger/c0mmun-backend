package at.htlkrems.c0mmun.service;

import at.htlkrems.c0mmun.Properties;
import at.htlkrems.c0mmun.domain.user.IInviteRepository;
import at.htlkrems.c0mmun.domain.user.IReportRepository;
import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.user.Invite;
import at.htlkrems.c0mmun.model.user.Report;
import at.htlkrems.c0mmun.model.user.User;
import at.htlkrems.c0mmun.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.validation.Valid;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
@RequestMapping("/Users")
public class UserService {

    Logger logger = LoggerFactory.getLogger(UserService.class);

    //region User
    @Autowired
    @Qualifier("customUserService")
    UserServiceImpl userService;

    @Autowired
    private IUserRepository userRepository;



    @DeleteMapping(path = "/DeleteAccount/{userId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable("userId") Long id)
    {
        logger.info("Deleted User with Id: "+id);
        userService.delete(id);
    }


    @CrossOrigin(origins = Properties.baseUrl)
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping(path="/Username/{username}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public User getUserByUsername(@PathVariable("username") String username)
    {
        logger.info("Get User with Username: " + username);

        return userRepository.findOne(username);
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping(path="/{userid}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public User getUserById(@PathVariable("userid") Long id)
    {
        logger.info("Get User with id: " + id);

    //endregion

    //region Invite

    @Autowired
    IInviteRepository inviteRepository;

    @PostMapping(path = "/CreateInvite", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Invite createInvite(@RequestBody Invite invite)
    {
        logger.info("Created Invite with Id: "+invite.getId());
        return inviteRepository.save(invite);
    }

    //endregion

    //region Authentication
    //endregion

    //region Report

    @Autowired
    IReportRepository reportRepository;

    @PostMapping(path = "/CreateReport", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Report CreateReport(@RequestBody Report report)
    {
        logger.info("Created Report with Id: " +report.getId());
        return reportRepository.save(report);
    }
    //endregion

}
