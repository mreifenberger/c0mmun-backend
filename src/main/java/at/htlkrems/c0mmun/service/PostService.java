package at.htlkrems.c0mmun.service;

import at.htlkrems.c0mmun.Properties;
import at.htlkrems.c0mmun.domain.comment.ICommentRepository;
import at.htlkrems.c0mmun.domain.post.IPostRepository;
import at.htlkrems.c0mmun.domain.tag.ITagRepository;
import at.htlkrems.c0mmun.domain.user.IRating_Post_UserRepository;
import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.DTOs.Comment_PostCreate;
import at.htlkrems.c0mmun.model.DTOs.Tag_PostCreate;
import at.htlkrems.c0mmun.model.post.Post;
import at.htlkrems.c0mmun.model.user.Rating_Post_User;
import at.htlkrems.c0mmun.model.user.User;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
@RequestMapping("/Posts")
@Transactional()
        //rollbackFor = Exception.class)
public class PostService {


    Logger logger = LoggerFactory.getLogger(PostService.class);

    @Autowired
    IRating_Post_UserRepository rating_post_userRepository;

    @Autowired
    ITagRepository tagRepository;

    @Autowired
    ICommentRepository commentRepository;

    @Autowired
    IUserRepository userRepository;

    @Autowired
    IPostRepository postRepository;

    //region Post
    @CrossOrigin(origins = Properties.baseUrl)
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @PostMapping(path = "/CreatePost", produces = {"application/json"},
            consumes = "multipart/form-data")
    @ResponseStatus(HttpStatus.CREATED)
    public Post createPost(@RequestPart("img") MultipartFile img, @RequestPart("post") Post post) throws IOException {
        logger.info("Created Post");
        try {
            post.getFile().setFile(img.getBytes());
        } catch (IOException e) {
            logger.info("Problems with converting file to byte[]");
        }

        Post post2 = postRepository.save(post);
        return post2;

    }

    @CrossOrigin(origins = Properties.baseUrl)
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @DeleteMapping(path = "/DeletePost/{postId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePost(@PathVariable("postId") Long id) {
        logger.info("Deleted Post with Id: " + id);
        postRepository.deleteById(id);
    }

    @CrossOrigin(origins = Properties.baseUrl)
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping(path = "/Post/{postId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Post getPost(@PathVariable("postId") Long id) {
        Post returnedPost = postRepository.FindPostsById(id);
        return returnedPost;
    }
    //endregion

    //region Searches

    @CrossOrigin(origins = Properties.baseUrl)
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping(path = "/User/{userId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Page<Post> profileSearch(@PathVariable("userId") Long user, Pageable pageable) {

        Page<Post> posts = postRepository.FindPostsByUser(user, pageable);

        logger.info("Getting Posts from User with Id: " + user);
        return posts;
    }

    @CrossOrigin(origins = Properties.baseUrl)
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping(path = "/Own/{userId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Page<Post> ownProfile(User user, PageRequest pageRequest) {
        throw new NotImplementedException();    //besprechen
    }

    @CrossOrigin(origins = Properties.baseUrl)
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping(path = "/Tag/{tagId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Page<Post> tagSearch(@PathVariable("tagId") Long tag, Pageable pageable) {
        logger.info("Getting Posts from Tag with Id: " + tag);
        return postRepository.FindPostsByTag(tag, pageable);
    }

    @CrossOrigin(origins = Properties.baseUrl)
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @PostMapping(path = "/AddTag", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Post addTag(@RequestBody Tag_PostCreate tag_postCreate) {
        tag_postCreate.getTag().getPosts().add(tag_postCreate.getPost());
        tag_postCreate.setTag(tagRepository.save(tag_postCreate.getTag()));

        tag_postCreate.getPost().getTags().add(tag_postCreate.getTag());

        tag_postCreate.setPost(postRepository.save(tag_postCreate.getPost()));

        return tag_postCreate.getPost();
    }


    @CrossOrigin(origins = Properties.baseUrl)          //Testen
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @PostMapping(path = "/AddComment", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Post addComment(@RequestBody Comment_PostCreate comment_postCreate) {
        commentRepository.save(comment_postCreate.getComment());
        comment_postCreate.getPost().getComments().add(comment_postCreate.getComment());
        return postRepository.save(comment_postCreate.getPost());
    }

    @CrossOrigin(origins = Properties.baseUrl)
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping(path = "/Category/{categoryId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Page<Post> CategorySearch(@PathVariable("categoryId") Long category, Pageable pageable) {
        logger.info("Getting Posts from Category with Id: " + category);
        List<Post> postsall = postRepository.GetAllWithAll();
        Page<Post> posts = postRepository.FindPostsByCategory(category, pageable);
        return posts;
    }

    //endregion

    //region Rating

    @CrossOrigin(origins = Properties.baseUrl)          //Testen
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @PostMapping(path = "/RatePost", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Rating_Post_User RatePost(@RequestBody Rating_Post_User rating_post_user) {
        logger.info("Creating Rating_Post_User with UserId:" + rating_post_user.getId().getUser().getId() + " and PostId: " + rating_post_user.getId().getPost().getId());



        Rating_Post_User ratDb=rating_post_userRepository.findPostsById(rating_post_user.getId());
       if(ratDb==null) {
           rating_post_user.getId().getPost().getUser().setRating(rating_post_user.getId().getPost().getUser().getRating() + rating_post_user.getValue());
           rating_post_user.getId().getPost().setRating(rating_post_user.getId().getPost().getRating() + rating_post_user.getValue());
           postRepository.save(rating_post_user.getId().getPost());
           userRepository.save(rating_post_user.getId().getPost().getUser());

           return rating_post_userRepository.save(rating_post_user);
       }
       else {
           if(ratDb.getValue()!=rating_post_user.getValue())
           {
               rating_post_user.getId().getPost().getUser().setRating(rating_post_user.getId().getPost().getUser().getRating() + rating_post_user.getValue());
               rating_post_user.getId().getPost().setRating(rating_post_user.getId().getPost().getRating() + rating_post_user.getValue());
               postRepository.save(rating_post_user.getId().getPost());
               userRepository.save(rating_post_user.getId().getPost().getUser());

               rating_post_user.getId().getPost().getUser().setRating(rating_post_user.getId().getPost().getUser().getRating() - ratDb.getValue());
               rating_post_user.getId().getPost().setRating(rating_post_user.getId().getPost().getRating() - ratDb.getValue());

               postRepository.save(rating_post_user.getId().getPost());
               userRepository.save(rating_post_user.getId().getPost().getUser());

               rating_post_userRepository.delete(ratDb);
               return rating_post_userRepository.save(rating_post_user);
           }
           else
           {
               return rating_post_user;
           }
       }

    }



    //endregion
}
