package at.htlkrems.c0mmun.service;

import at.htlkrems.c0mmun.domain.comment.ICommentRepository;
import at.htlkrems.c0mmun.domain.user.IRating_Comment_UserRepository;
import at.htlkrems.c0mmun.model.comment.Comment;
import at.htlkrems.c0mmun.model.user.Rating_Comment_User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController()
@RequestMapping("/Comments")
public class CommentService {

    Logger logger = LoggerFactory.getLogger(CommentService.class);

    @Autowired
    IRating_Comment_UserRepository rating_comment_userRepository;

    @Autowired
    private ICommentRepository commentRepository;


    //region Comment

    @PostMapping(path = "/CreateComment",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Comment createComment(@RequestBody Comment comment)
    {
        logger.info("Created Comment with Id: "+comment.getId());
        return commentRepository.save(comment);
    }

    @DeleteMapping(path = "/DeleteComment/{commentId}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteComment(@PathVariable("commentId") Long id)
    {
        logger.info("Deleted Comment with Id: "+id);
        commentRepository.deleteById(id);
    }

    @GetMapping(path = "/Comment/{commentId}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Comment getComment(@PathVariable("commentId") Long id)
    {
        return commentRepository.getOne(id);
    }
    //endregion

    //region Rating

    @PostMapping(path = "/RateComment",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Rating_Comment_User RateComment(@RequestBody Rating_Comment_User rating_comment_user)
    {
        rating_comment_user.setDate(new Date());
        logger.info("Creating Rating_Comment_User with UserId:"+ rating_comment_user.getId().getUser().getId()+" and TagId: "+ rating_comment_user.getId().getComment().getId());
        return rating_comment_userRepository.save(rating_comment_user);
    }




    //endregion



}
