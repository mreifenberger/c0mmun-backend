package at.htlkrems.c0mmun.service.impl;

import at.htlkrems.c0mmun.DTO.UserDTO;
import at.htlkrems.c0mmun.config.UserService;
import at.htlkrems.c0mmun.domain.user.IRolesRepository;
import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.user.Role;
import at.htlkrems.c0mmun.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service(value = "customUserService")
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private IUserRepository userRepo;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    @Autowired
    private IRolesRepository rolesRepository;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        user.getRoles().forEach(role -> {
            //authorities.add(new SimpleGrantedAuthority(role.getName()));
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        });
        return authorities;
        //return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }


    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        userRepo.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(Long id) {
        userRepo.deleteById(id);
    }

    @Override
    public User findOne(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public User update(User user) {
        return userRepo.save(user);
    }

    @Override
    public User findById(Long id) {
        return userRepo.findById(id).get();
    }

    @Override
    public User save(UserDTO user) {
        User newUser = new User();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser .setEmail(user.getEmail());
        newUser.setRating(0l);
        Role r = rolesRepository.findByName("USER");
        newUser.getRoles().add(r);
        return userRepo.save(newUser);
    }

}
