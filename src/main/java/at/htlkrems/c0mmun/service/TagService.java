package at.htlkrems.c0mmun.service;

import at.htlkrems.c0mmun.domain.tag.ITagRepository;
import at.htlkrems.c0mmun.domain.user.IRating_Tag_UserRepository;
import at.htlkrems.c0mmun.model.tag.Tag;
import at.htlkrems.c0mmun.model.user.Rating_Tag_User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController()
@RequestMapping("/Tags")
public class TagService {

    Logger logger = LoggerFactory.getLogger(TagService.class);

    @Autowired
    IRating_Tag_UserRepository rating_tag_userRepository;

    @Autowired
    private ITagRepository tagRepository;

    //region Tag
    @PostMapping(path = "/CreateTag",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Tag createTag(@RequestBody Tag tag)
    {
        logger.info("Created Tag with Id: "+tag.getId());
        try {
            tagRepository.save(tag);
        }
        catch (DataIntegrityViolationException e) {
            System.out.println("tag already exist");
        }

        return tag;
    }

    @DeleteMapping(path = "/DeleteTag/{tagId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTag(@PathVariable("tagId") Long id)
    {
        logger.info("Deleted Tag with Id: "+id);
        tagRepository.deleteById(id);
    }

    @GetMapping(path = "/Tag/{tagId}")
    @ResponseStatus(HttpStatus.OK)
    public Tag getTag(@PathVariable("tagId") Long id)
    {
        return tagRepository.getOne(id);
    }
    //endregion

    //region Rating

    @PostMapping(path = "/RateTag",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Rating_Tag_User RateTag(@RequestBody Rating_Tag_User rating_tag_user)
    {
        rating_tag_user.setDate(new Date());
        logger.info("Creating Rating_Tag_User with UserId:"+ rating_tag_user.getId().getUser().getId()+" and TagId: "+ rating_tag_user.getId().getTag().getId());
        return rating_tag_userRepository.save(rating_tag_user);
    }




    //endregion



}
