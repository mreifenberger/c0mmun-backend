package at.htlkrems.c0mmun.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MailInviteDTO implements Serializable {

    private String message;

    private String mail;

}
