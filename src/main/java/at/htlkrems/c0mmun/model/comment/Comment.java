package at.htlkrems.c0mmun.model.comment;

import at.htlkrems.c0mmun.model.AEntity;
import at.htlkrems.c0mmun.model.user.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Comments")
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Comment extends AEntity {

    @NotBlank
    @Column(name = "text", nullable = false)
    private String text;

    //@JsonBackReference(value = "comment")
    @ManyToOne()
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn()
    private List<Comment> childComments = new ArrayList<>();

}
