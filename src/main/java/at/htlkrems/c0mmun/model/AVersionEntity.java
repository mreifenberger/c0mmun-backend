package at.htlkrems.c0mmun.model;

import lombok.Getter;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.io.Serializable;

@MappedSuperclass
public class AVersionEntity implements Serializable {

    @Version
    @Getter
    private Long version;

}
