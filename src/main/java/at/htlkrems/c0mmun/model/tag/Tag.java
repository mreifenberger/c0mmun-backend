package at.htlkrems.c0mmun.model.tag;

import at.htlkrems.c0mmun.model.AEntity;
import at.htlkrems.c0mmun.model.post.Post;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Tags")
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Tag extends AEntity {

    @NotBlank
    @Size(max = 50)
    @Column(name ="text", nullable = false,length = 50,unique = true)
    private String text;

    @Column(name = "description")
    private  String description;

    @JsonIgnore()
    @ManyToMany(mappedBy = "tags")
    private List<Post> posts= new ArrayList<>();
}
