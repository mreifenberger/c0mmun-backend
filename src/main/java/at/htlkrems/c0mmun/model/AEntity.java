package at.htlkrems.c0mmun.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@MappedSuperclass
public class AEntity implements Serializable {

    @SequenceGenerator(name = "mySeqGen", sequenceName = "mySeq", initialValue = 20, allocationSize = 100)
    @GeneratedValue(generator = "mySeqGen")
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

}
