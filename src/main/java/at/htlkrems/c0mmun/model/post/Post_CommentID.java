package at.htlkrems.c0mmun.model.post;

import at.htlkrems.c0mmun.model.comment.Comment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Post_CommentID implements Serializable {

    @ManyToOne
    @JoinColumn(name="post_id",nullable = false,updatable = false)
    private at.htlkrems.c0mmun.model.post.Post post;

    @ManyToOne
    @JoinColumn(name="comment_id",nullable = false,updatable = false)
    private Comment comment;
}
