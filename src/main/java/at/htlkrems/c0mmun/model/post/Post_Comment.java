package at.htlkrems.c0mmun.model.post;

import at.htlkrems.c0mmun.model.AVersionEntity;
import at.htlkrems.c0mmun.model.comment.Comment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Post_Comment")
@Entity
public class Post_Comment extends AVersionEntity {
    @NotNull
    @EmbeddedId
    private Post_CommentID id;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", nullable = false)
    private Date date;

    public Post_Comment(Post post, Comment comment) {
        id = new Post_CommentID(post,comment);
    }
}
