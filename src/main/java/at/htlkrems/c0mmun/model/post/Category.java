package at.htlkrems.c0mmun.model.post;

import at.htlkrems.c0mmun.model.AEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Categories")
@Entity
public class Category extends AEntity {

    @NotBlank
    @Column(name="name",nullable = false)
    private String name;

}
