package at.htlkrems.c0mmun.model.post;

import at.htlkrems.c0mmun.model.AEntity;
import at.htlkrems.c0mmun.model.comment.Comment;
import at.htlkrems.c0mmun.model.tag.Tag;
import at.htlkrems.c0mmun.model.user.User;
import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Posts")
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class,property="id", scope = Post.class)
@NamedEntityGraph(
        name = "post-with-data",
        attributeNodes = {
                @NamedAttributeNode("name"),
                @NamedAttributeNode(value = "file",subgraph = "load-file"),
                @NamedAttributeNode("tags"),
                @NamedAttributeNode("categories"),
                @NamedAttributeNode("user")
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "load-file",
                        attributeNodes = {
                                @NamedAttributeNode("file"),
                                @NamedAttributeNode("uploadDate")
                        })
        }

)
public class Post extends AEntity {


    @Column(name="name",nullable = false)
    private String name;

    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    private UploadedFile file = new UploadedFile();

    @NotNull
    @Column(name="rating", nullable = false)
    private Long rating=0l;

    @ManyToMany()
    @JoinTable( name="post_tag",
            foreignKey = @ForeignKey(name = "post_id"),
            inverseForeignKey = @ForeignKey(name = "tag_id"))
    @OrderColumn
    private List<Tag> tags = new ArrayList<>();

    @ManyToMany
    private List<Category> categories = new ArrayList<>();

    @OneToMany
    private List<Comment> comments = new ArrayList<>();

    //@JsonManagedReference(value = "post")
    @ManyToOne
    @JoinColumn(name = "user")
    private User user;

}
