package at.htlkrems.c0mmun.model.post;

import at.htlkrems.c0mmun.model.AEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Getter
@Setter
@Entity
@Table(name = "uploaded_files")
public class UploadedFile extends AEntity {

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(length = 16777214)
    private byte[] file;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "upload_date", nullable = false)
    private Date uploadDate;
}
