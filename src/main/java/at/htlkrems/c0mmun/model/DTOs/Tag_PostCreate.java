package at.htlkrems.c0mmun.model.DTOs;

import at.htlkrems.c0mmun.model.post.Post;
import at.htlkrems.c0mmun.model.tag.Tag;
import lombok.Data;

import java.io.Serializable;

@Data
public class Tag_PostCreate implements Serializable {

    private Tag tag;

    private Post post;
}
