package at.htlkrems.c0mmun.model.DTOs;

import at.htlkrems.c0mmun.model.comment.Comment;
import at.htlkrems.c0mmun.model.post.Post;
import lombok.Data;

import java.io.Serializable;

@Data
public class Comment_PostCreate implements Serializable {

    private Comment comment;

    private Post post;
}