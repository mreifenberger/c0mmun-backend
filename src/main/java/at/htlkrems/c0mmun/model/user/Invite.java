package at.htlkrems.c0mmun.model.user;

import at.htlkrems.c0mmun.model.AEntity;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="Invites")
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Invite extends AEntity {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user",nullable = false)
    private User user;

    @NotBlank
    @Email
    @Column(name = "email", nullable = false)
    private String email;
}
