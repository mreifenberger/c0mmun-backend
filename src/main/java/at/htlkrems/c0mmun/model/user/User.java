package at.htlkrems.c0mmun.model.user;

import at.htlkrems.c0mmun.model.AEntity;
import at.htlkrems.c0mmun.model.Code;
import at.htlkrems.c0mmun.model.comment.Comment;
import at.htlkrems.c0mmun.model.post.Post;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="Users")
@Entity
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class,property="id", scope = User.class)
public class User extends AEntity {

    @NotBlank
    @Email
    @Size(max = 100)
    @Column(name = "email", nullable = false,length = 100,unique = true)
    private String email;

    @NotBlank
    @Size(max = 50)
    @Column(name="username",nullable = false, length = 50,unique = true)
    private String username;

    @NotBlank
    @Column(name="password", nullable = false)
    private String password;                              //??

    @NotNull
    @Column(name="rating", nullable = false)
    private Long rating=0l;


    @JsonBackReference(value = "post")
    @OneToMany(mappedBy = "user")
    private List<Post> posts= new ArrayList<>();

    @JsonBackReference(value = "comment")
    @OneToMany(mappedBy = "user")
    private List<Comment> comments = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    private List<Invite> invites= new ArrayList<>();

    @Column(name = "inviteCount")
    @Min(0)
    @NotNull
    private Integer inviteCount = 0;

    @OneToMany(mappedBy = "reported")
    private List<Report> recievedReports= new ArrayList<>();

    @OneToMany(mappedBy = "reporter")
    private List<Report> reports= new ArrayList<>();

    @Column(name = "active")
    private Boolean active;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "USER_ROLES", joinColumns = {
            @JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
            @JoinColumn(name = "ROLE_ID") })
    private List<Role> roles = new ArrayList<>();

    @OneToMany
    private List<Code> codes = new ArrayList<>();



    public User(String email,String username,String password,Long rating,List<Post> posts,List<Comment> comments,Boolean active) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.rating = rating;
        this.posts = posts;
        this.comments = comments;
        this.active = active;
    }

    public User(long id) {
        this.setId(id);
    }

}
