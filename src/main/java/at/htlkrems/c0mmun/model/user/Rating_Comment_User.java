package at.htlkrems.c0mmun.model.user;

import at.htlkrems.c0mmun.model.AVersionEntity;
import at.htlkrems.c0mmun.model.comment.Comment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Likes_Comment_User")
@Entity
public class Rating_Comment_User extends AVersionEntity {

    @NotNull
    @EmbeddedId
    private Rating_Comment_UserID id;

    @NotNull
    @Column(name="value",nullable = false)
    private int value;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", nullable = false)
    private Date date;



    public Rating_Comment_User(Comment comment, User user) {
        id = new Rating_Comment_UserID(comment,user);
    }
}
