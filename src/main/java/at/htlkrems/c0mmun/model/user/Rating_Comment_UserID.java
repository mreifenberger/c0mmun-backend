package at.htlkrems.c0mmun.model.user;

import at.htlkrems.c0mmun.model.comment.Comment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Rating_Comment_UserID implements Serializable {

    @ManyToOne
    @JoinColumn(name="comment_id",nullable = false,updatable = false)
    private Comment comment;

    @ManyToOne
    @JoinColumn(name="user_id",nullable = false,updatable = false)
    private User user;

}
