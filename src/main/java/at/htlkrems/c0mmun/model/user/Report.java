package at.htlkrems.c0mmun.model.user;

import at.htlkrems.c0mmun.model.AEntity;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="Reports")
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Report extends AEntity {

    @ManyToOne
    @JoinColumn(name ="reporter", nullable = false)
    private User reporter;

    @NotBlank
    @Column(name = "description",nullable = false)
    private String description;

    @ManyToOne
    @JoinColumn(name = "reported", nullable = false)
    private User reported;
}
