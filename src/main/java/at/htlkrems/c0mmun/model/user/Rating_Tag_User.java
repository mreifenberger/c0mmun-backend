package at.htlkrems.c0mmun.model.user;

import at.htlkrems.c0mmun.model.AVersionEntity;
import at.htlkrems.c0mmun.model.tag.Tag;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Likes_Tag_User")
@Entity
public class Rating_Tag_User extends AVersionEntity {

    @NotNull
    @EmbeddedId
    private Rating_Tag_UserID id;

    @NotNull
    @Column(name="value",nullable = false)
    private int value;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", nullable = false)
    private Date date;



    public Rating_Tag_User(Tag tag, User user) {
        id = new Rating_Tag_UserID(tag,user);
    }
}
