package at.htlkrems.c0mmun.model.user;

import at.htlkrems.c0mmun.model.AVersionEntity;
import at.htlkrems.c0mmun.model.post.Post;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Rating_Post_User")
@Entity
public class Rating_Post_User extends AVersionEntity {

    @NotNull
    @EmbeddedId
    private Rating_Post_UserID id;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", nullable = false)
    private Date date;

    @NotNull
    @Column(name="value",nullable = false)
    private int value;

    public Rating_Post_User(Post post, User user) {
        id = new Rating_Post_UserID(post,user);
    }
}
