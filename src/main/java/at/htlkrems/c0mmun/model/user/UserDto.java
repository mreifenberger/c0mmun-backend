package at.htlkrems.c0mmun.model.user;

import at.htlkrems.c0mmun.model.comment.Comment;
import at.htlkrems.c0mmun.model.post.Post;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class UserDto {

    private String username;

    private Long rating=0l;

    private List<Post> posts= new ArrayList<>();

    private List<Comment> comments = new ArrayList<>();

    private Boolean active;

    private List<Role> roles;

}
