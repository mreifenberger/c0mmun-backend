package at.htlkrems.c0mmun.domain;

import at.htlkrems.c0mmun.model.Code;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ICodeRepository extends JpaRepository<Code, Long> {

    @Query("select c from Code c where c.Code = :code")
    Code findByCode(@Param("code") String code);

}
