package at.htlkrems.c0mmun.domain.user;

import at.htlkrems.c0mmun.model.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRolesRepository extends JpaRepository<Role,Long> {

    Role findByName(String name);

}
