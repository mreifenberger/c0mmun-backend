package at.htlkrems.c0mmun.domain.user;

import at.htlkrems.c0mmun.model.user.Rating_Tag_User;
import at.htlkrems.c0mmun.model.user.Rating_Tag_UserID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRating_Tag_UserRepository extends JpaRepository<Rating_Tag_User, Rating_Tag_UserID> {
}
