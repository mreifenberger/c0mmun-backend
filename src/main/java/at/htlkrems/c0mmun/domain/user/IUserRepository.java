package at.htlkrems.c0mmun.domain.user;

import at.htlkrems.c0mmun.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<User,Long> {


    User findByUsername(String username);
}
