package at.htlkrems.c0mmun.domain.user;

import at.htlkrems.c0mmun.model.user.Invite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IInviteRepository extends JpaRepository<Invite,Long> {
}
