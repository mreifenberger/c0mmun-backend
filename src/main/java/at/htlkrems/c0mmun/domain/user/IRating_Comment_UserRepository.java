package at.htlkrems.c0mmun.domain.user;

import at.htlkrems.c0mmun.model.user.Rating_Comment_User;
import at.htlkrems.c0mmun.model.user.Rating_Comment_UserID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRating_Comment_UserRepository extends JpaRepository<Rating_Comment_User, Rating_Comment_UserID> {
}
