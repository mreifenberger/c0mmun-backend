package at.htlkrems.c0mmun.domain.user;

import at.htlkrems.c0mmun.model.user.Report;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IReportRepository extends JpaRepository<Report,Long> {
}
