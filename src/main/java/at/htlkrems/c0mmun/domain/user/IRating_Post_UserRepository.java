package at.htlkrems.c0mmun.domain.user;

import at.htlkrems.c0mmun.model.post.Post;
import at.htlkrems.c0mmun.model.user.Rating_Post_User;
import at.htlkrems.c0mmun.model.user.Rating_Post_UserID;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IRating_Post_UserRepository extends JpaRepository<Rating_Post_User, Rating_Post_UserID> {

    @Query("select p from Rating_Post_User p where p.id = :id")
    Rating_Post_User findPostsById(@Param("id") Rating_Post_UserID id);
}
