package at.htlkrems.c0mmun.domain.tag;

import at.htlkrems.c0mmun.model.tag.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITagRepository extends JpaRepository<Tag,Long> {
}
