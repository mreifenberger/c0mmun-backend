package at.htlkrems.c0mmun.domain.post;

import at.htlkrems.c0mmun.model.post.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICategoryRepository extends JpaRepository<Category,Long> {

}
