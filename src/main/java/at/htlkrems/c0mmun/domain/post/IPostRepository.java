package at.htlkrems.c0mmun.domain.post;

import at.htlkrems.c0mmun.model.post.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IPostRepository extends JpaRepository<Post,Long> {

    @EntityGraph(value="post-with-data",type = EntityGraph.EntityGraphType.FETCH)
    @Query("select p from Post p")
    List<Post> GetAllWithAll();

    @EntityGraph(value="post-with-data",type = EntityGraph.EntityGraphType.FETCH)
    @Query("select p from Post p where p.id = :id")
    Post FindPostsById(@Param("id") Long id);

    @Query("select p from Post p where p.user.id = :userid")
    Page<Post> FindPostsByUser(@Param("userid") Long userid, Pageable pageable);

    @Query("select t.posts from Tag t where t.id = :tagid")
    Page<Post> FindPostsByTag(@Param("tagid") Long tagid, Pageable pageable);

    @Query("select p from Post p join p.categories c where c.id = :categoryid")
    Page<Post> FindPostsByCategory(@Param("categoryid") Long categoryid, Pageable pageable);


}
