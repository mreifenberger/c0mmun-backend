package at.htlkrems.c0mmun.domain.post;

import at.htlkrems.c0mmun.model.post.Post_Comment;
import at.htlkrems.c0mmun.model.post.Post_CommentID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPost_CommentRepository extends JpaRepository<Post_Comment, Post_CommentID> {
}
