package at.htlkrems.c0mmun.domain.comment;

import at.htlkrems.c0mmun.model.comment.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICommentRepository extends JpaRepository<Comment,Long> {
}
