package at.htlkrems.c0mmun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class C0mmunApplication {

		public static void main(String[] args) {
			SpringApplication.run(C0mmunApplication.class, args);
	}
}