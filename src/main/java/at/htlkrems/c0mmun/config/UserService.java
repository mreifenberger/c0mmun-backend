package at.htlkrems.c0mmun.config;

import at.htlkrems.c0mmun.DTO.UserDTO;
import at.htlkrems.c0mmun.model.user.User;

import java.util.List;

public interface UserService {

    User save(UserDTO user);
    List<User> findAll();
    void delete(Long id);

    User findOne(String username);

    User findById(Long id);

    User update(User userDto);
}

