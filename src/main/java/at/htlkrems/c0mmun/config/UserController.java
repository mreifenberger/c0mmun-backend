package at.htlkrems.c0mmun.config;

import at.htlkrems.c0mmun.DTO.UserDTO;
import at.htlkrems.c0mmun.domain.ICodeRepository;
import at.htlkrems.c0mmun.model.Code;
import at.htlkrems.c0mmun.model.MailInviteDTO;
import at.htlkrems.c0mmun.model.user.User;
import at.htlkrems.c0mmun.service.impl.UserServiceImpl;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.annotation.*;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private ICodeRepository codeRepository;

    //@Secured({"ROLE_ADMIN", "ROLE_USER"})
    @CrossOrigin(origins = "http://localhost:4200")
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/user", method = RequestMethod.GET)
    public List listUser(){
        return userService.findAll();
    }

    //@Secured("ROLE_USER")
    //@PreAuthorize("hasRole('USER')")
    @CrossOrigin(origins = "http://localhost:4200")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public User getOne(@PathVariable(value = "id") Long id){
        return userService.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value="/signup", method = RequestMethod.POST)
    public User create(@RequestBody UserDTO user){

        String code = user.getCode();

        Code c = codeRepository.findByCode(code);
        if(c != null)
        {
            codeRepository.delete(c);
            return userService.save(user);
        }

        return null;
    }


    @PreAuthorize("hasRole('ADMIN')")
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value="/user/{id}", method = RequestMethod.DELETE)
    public User deleteUser(@PathVariable(value = "id") Long id){
        userService.delete(id);
        return new User(id);
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value="/sendmail", method = RequestMethod.POST)
    public String mail(@RequestBody MailInviteDTO dto) throws IOException, MessagingException {
        Code code = codeRepository.save(new Code(createCode()));
        return sendmail(dto, code);
    }


    @Autowired
    private JavaMailSender sender;


    private String sendmail(MailInviteDTO mailclient, Code code) throws AddressException, MessagingException, IOException {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {
            helper.setTo(mailclient.getMail());
            helper.setText("Du wurdest eingeladen c0mmun zu nutzen. Gib bei der Registrierung folgenden Code an:" + code.getCode() +
                            "Dein Freund hat dir folgende Nachricht hinterlassen: " + mailclient.getMessage());
            helper.setSubject("Deine Einladung zu c0mmun");
        } catch (MessagingException e) {
            return "Error while sending mail ..";
        }
        sender.send(message);
        return "Mail Sent Success!";
    }

    private String createCode() {
        return RandomStringUtils.randomAlphanumeric(20);

    }

}
