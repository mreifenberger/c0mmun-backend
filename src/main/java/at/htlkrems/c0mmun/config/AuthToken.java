package at.htlkrems.c0mmun.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthToken {


    private String token;
    private String username;

    public AuthToken(){

    }

    public AuthToken(String token, String username){
        this.token = token;
        this.username = username;
    }

    public AuthToken(String token){
        this.token = token;
    }


}
