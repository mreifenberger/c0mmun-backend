package at.htlkrems.c0mmun.tag;

import at.htlkrems.c0mmun.domain.tag.ITagRepository;
import at.htlkrems.c0mmun.model.tag.Tag;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@WebAppConfiguration
@Transactional
public class TagUnitTest {

    @Autowired
    private ITagRepository tagRepository;

    @Test
    public void createTag() {
        Tag tag = tagEntityFactory.createTagData();
        Tag savedEntity = tagRepository.save(tag);

        assertNotNull(savedEntity.getId());
        assertEquals(tag.getText(), savedEntity.getText());
    }

    @Test
    public void updateTag(){
        Tag tag = tagEntityFactory.createTagData();
        Tag savedEntity = tagRepository.save(tag);

        savedEntity= tagEntityFactory.updateTagData(savedEntity);
        Tag savedEntity2 = tagRepository.save(savedEntity);

        assertEquals(savedEntity2.getText(), savedEntity.getText());
    }

    @Test
    public void deleteTag() {
        Tag tag = tagEntityFactory.createTagData();
        Tag savedEntity = tagRepository.save(tag);

        assertNotNull(savedEntity.getId());
        assertEquals(tag.getText(), savedEntity.getText());

        tagRepository.delete(savedEntity);

        assertFalse(tagRepository.existsById(savedEntity.getId()));
    }
}
