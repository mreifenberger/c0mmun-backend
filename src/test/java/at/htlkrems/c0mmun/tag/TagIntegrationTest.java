package at.htlkrems.c0mmun.tag;


import at.htlkrems.c0mmun.domain.post.ICategoryRepository;
import at.htlkrems.c0mmun.domain.post.IPostRepository;
import at.htlkrems.c0mmun.domain.tag.ITagRepository;
import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.tag.Tag;
import at.htlkrems.c0mmun.model.user.Rating_Tag_User;
import at.htlkrems.c0mmun.model.user.User;
import at.htlkrems.c0mmun.user.userEntityFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class TagIntegrationTest {

    @Autowired
    private IPostRepository postRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private ITagRepository tagRepository;

    @Autowired
    private ICategoryRepository categoryRepository;

    private static final String URL_TEMPLATE = "%s/%s/%s";

    private static final String ADVANCED_URL_TEMPLATE = "%s/%s/%s/%s";

    private String serverURL = "http://127.0.0.1:8080";

    private String contextRoot = "Tags";


    @Test
    public void createTag() {


        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "CreateTag");

        System.out.println(requestURL);

        RestTemplate restClient = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));

        Tag tag = tagEntityFactory.createTagData();

        HttpEntity<Tag> request = new HttpEntity<>(tag, headers);
        ResponseEntity<Tag> response = restClient.postForEntity(requestURL, request, Tag.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        Tag createdTag = response.getBody();
        assertNotNull(createdTag.getId());
    }

    @Test
    public void deletePost() {


        Tag tag = tagEntityFactory.createTagData();
        tag= tagRepository.save(tag);

        assertNotNull(tag.getId());

        System.out.println("TAG_ID : "+tag.getId());

        String requestURL = String.format(ADVANCED_URL_TEMPLATE, serverURL, contextRoot, "DeleteTag",tag.getId());

        System.out.println(requestURL);

        RestTemplate restClient = new RestTemplate();

        restClient.delete(requestURL);
    }


    @Test
    public void getPost() {
        Tag tag = tagEntityFactory.createTagData();
        tag= tagRepository.save(tag);

        assertNotNull(tag.getId());

        System.out.println("TAG_ID : "+tag.getId());

        String requestURL = String.format(ADVANCED_URL_TEMPLATE, serverURL, contextRoot, "Tag",tag.getId());

        System.out.println(requestURL);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restClient = new RestTemplate();

        //HttpEntity<Comment> request = new HttpEntity<>(comment, headers);

        //ResponseEntity<Comment> response = restClient.exchange(requestURL, HttpMethod.GET, request, Comment.class);


        //Comment getComment = response.getBody();



        //getComment= restClient.getForObject(requestURL, Comment.class);


        ResponseEntity<Tag> httpResponse = restClient.getForEntity(requestURL, Tag.class);

        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);

        Tag getTag = httpResponse.getBody();
        assertEquals(getTag.getId(), tag.getId());
    }



    @Test
    public void rateTag() {
        Tag tag = tagEntityFactory.createTagData();
        tag= tagRepository.save(tag);

        assertNotNull(tag.getId());

        System.out.println("TAG_ID : "+tag.getId());

        User user  = userEntityFactory.createUserData();
        user= userRepository.save(user);

        System.out.println("USER_ID : "+user.getId());

        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "RateTag");

        System.out.println(requestURL);

        Rating_Tag_User rating= userEntityFactory.createRating_Tag_UserData(tag,user);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<Rating_Tag_User> request = new HttpEntity<>(rating, headers);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restClient = new RestTemplate();

        //HttpEntity<Comment> request = new HttpEntity<>(comment, headers);

        //ResponseEntity<Comment> response = restClient.exchange(requestURL, HttpMethod.GET, request, Comment.class);


        //Comment getComment = response.getBody();



        //getComment= restClient.getForObject(requestURL, Comment.class);


        ResponseEntity<Rating_Tag_User> httpResponse = restClient.postForEntity(requestURL,request, Rating_Tag_User.class);

        assertEquals(httpResponse.getStatusCode(), HttpStatus.CREATED);

        Rating_Tag_User getRating_Tag_User = httpResponse.getBody();
        assertNotNull(rating.getId());
    }

}


