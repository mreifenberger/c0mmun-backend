package at.htlkrems.c0mmun.tag;

import at.htlkrems.c0mmun.model.tag.Tag;

public class tagEntityFactory {

    static int count=0;
    public static Tag createTagData(){

        Tag tag = new Tag();
        tag.setText("comment"+count);
        tag.setDescription("desc");
        count++;

        return tag;
    }

    public static Tag updateTagData(Tag savedEntity) {
        savedEntity.setText("updated text");

        return savedEntity;
    }
}
