package at.htlkrems.c0mmun.user;

import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@WebAppConfiguration
@Transactional
public class UserUnitTest {

    @Autowired
    private IUserRepository userRepository;

    @Test
    public void createUser() {
        User user = userEntityFactory.createUserData();
        User savedEntity = userRepository.save(user);

        assertNotNull(savedEntity.getId());
        assertEquals(user.getUsername(), savedEntity.getUsername());
    }

    @Test
    public void updateUser(){
        User user = userEntityFactory.createUserData();
        User savedEntity = userRepository.save(user);

        savedEntity= userEntityFactory.updateUserData(savedEntity);
        User savedEntity2 = userRepository.save(savedEntity);

        assertEquals(savedEntity2.getUsername(), savedEntity.getUsername());
    }

    @Test
    public void deleteUser() {
        User user = userEntityFactory.createUserData();
        User savedEntity = userRepository.save(user);

        assertNotNull(savedEntity.getId());
        assertEquals(user.getUsername(), savedEntity.getUsername());

        userRepository.delete(savedEntity);

        assertFalse(userRepository.existsById(savedEntity.getId()));
    }
}
