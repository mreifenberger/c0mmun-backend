package at.htlkrems.c0mmun.user;

import at.htlkrems.c0mmun.domain.tag.ITagRepository;
import at.htlkrems.c0mmun.domain.user.IRating_Tag_UserRepository;
import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.user.Rating_Tag_User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static at.htlkrems.c0mmun.tag.tagEntityFactory.createTagData;
import static at.htlkrems.c0mmun.user.userEntityFactory.createUserData;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@WebAppConfiguration
@Transactional
public class Rating_Tag_UserUnitTest {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private ITagRepository tagRepository;

    @Autowired
    private IRating_Tag_UserRepository rating_tag_userRepository;

    @Test
    public void createLikes_Tag_User() {
        Rating_Tag_User rating_tag_user = userEntityFactory.createRating_Tag_UserData(
          tagRepository.save(createTagData()),
                userRepository.save(createUserData()));
        Rating_Tag_User savedEntity = rating_tag_userRepository.save(rating_tag_user);

        assertNotNull(savedEntity.getId());
        assertEquals(rating_tag_user.getDate(), savedEntity.getDate());
    }

    @Test
    public void updateLikes_Comment_User(){
        Rating_Tag_User rating_tag_user = userEntityFactory.createRating_Tag_UserData(
                tagRepository.save(createTagData()),
                userRepository.save(createUserData()));
        Rating_Tag_User savedEntity = rating_tag_userRepository.save(rating_tag_user);

        savedEntity= userEntityFactory.updateRating_Tag_UserData(savedEntity);
        Rating_Tag_User savedEntity2 = rating_tag_userRepository.save(savedEntity);

        assertEquals(savedEntity2.getDate(), savedEntity.getDate());
    }

    @Test
    public void deleteLikes_Comment_User() {
        Rating_Tag_User rating_tag_user = userEntityFactory.createRating_Tag_UserData(
                tagRepository.save(createTagData()),
                userRepository.save(createUserData()));
        Rating_Tag_User savedEntity = rating_tag_userRepository.save(rating_tag_user);

        assertNotNull(savedEntity.getId());
        assertEquals(rating_tag_user.getDate(), savedEntity.getDate());

        rating_tag_userRepository.delete(savedEntity);

        assertFalse(rating_tag_userRepository.existsById(savedEntity.getId()));
    }
}
