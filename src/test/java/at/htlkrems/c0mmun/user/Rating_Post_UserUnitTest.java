package at.htlkrems.c0mmun.user;

import at.htlkrems.c0mmun.domain.post.IPostRepository;
import at.htlkrems.c0mmun.domain.user.IRating_Post_UserRepository;
import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.user.Rating_Post_User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static at.htlkrems.c0mmun.post.postEntityFactory.createPostData;
import static at.htlkrems.c0mmun.user.userEntityFactory.createUserData;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@WebAppConfiguration
@Transactional
public class Rating_Post_UserUnitTest {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IPostRepository postRepository;

    @Autowired
    private IRating_Post_UserRepository likes_post_userRepository;

    @Test
    public void createLikes_Post_User() {
        Rating_Post_User rating_post_userData = userEntityFactory.createLikes_Post_UserData(
                postRepository.save(createPostData()),
                userRepository.save(createUserData()));
        Rating_Post_User savedEntity = likes_post_userRepository.save(rating_post_userData);

        assertNotNull(savedEntity.getId());
        assertEquals(rating_post_userData.getDate(), savedEntity.getDate());
    }

    @Test
    public void updateLikes_Post_User(){
        Rating_Post_User rating_post_userData = userEntityFactory.createLikes_Post_UserData(
                postRepository.save(createPostData()),
                userRepository.save(createUserData()));
        Rating_Post_User savedEntity = likes_post_userRepository.save(rating_post_userData);

        savedEntity= userEntityFactory.updateLikes_Post_UserData(savedEntity);
        Rating_Post_User savedEntity2 = likes_post_userRepository.save(savedEntity);

        assertEquals(savedEntity2.getDate(), savedEntity.getDate());
    }

    @Test
    public void deleteLikes_Post_User() {
        Rating_Post_User rating_post_userData = userEntityFactory.createLikes_Post_UserData(
                postRepository.save(createPostData()),
                userRepository.save(createUserData()));
        Rating_Post_User savedEntity = likes_post_userRepository.save(rating_post_userData);

        assertNotNull(savedEntity.getId());
        assertEquals(rating_post_userData.getDate(), savedEntity.getDate());

        likes_post_userRepository.delete(savedEntity);

        assertFalse(likes_post_userRepository.existsById(savedEntity.getId()));
    }
}
