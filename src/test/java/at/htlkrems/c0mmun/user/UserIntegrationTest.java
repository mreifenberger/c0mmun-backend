package at.htlkrems.c0mmun.user;


import at.htlkrems.c0mmun.domain.user.IInviteRepository;
import at.htlkrems.c0mmun.domain.user.IReportRepository;
import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.user.Invite;
import at.htlkrems.c0mmun.model.user.Report;
import at.htlkrems.c0mmun.model.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class UserIntegrationTest {

    @Autowired
    private IInviteRepository inviteRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IReportRepository reportRepository;

    private static final String URL_TEMPLATE = "%s/%s/%s";

    private static final String ADVANCED_URL_TEMPLATE = "%s/%s/%s/%s";

    private String serverURL = "http://127.0.0.1:8080";

    private String contextRoot = "Users";


    @Test
    public void createUser() {


        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "CreateAccount");

        System.out.println(requestURL);

        RestTemplate restClient = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));

        User user = userEntityFactory.createUserData();

        HttpEntity<User> request = new HttpEntity<>(user, headers);
        ResponseEntity<User> response = restClient.postForEntity(requestURL, request, User.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        User createdUser = response.getBody();
        assertNotNull(createdUser.getId());
    }

    @Test
    public void deleteUser() {


        User user = userEntityFactory.createUserData();
        user= userRepository.save(user);

        assertNotNull(user.getId());

        System.out.println("USER_ID : "+user.getId());

        String requestURL = String.format(ADVANCED_URL_TEMPLATE, serverURL, contextRoot, "DeleteAccount",user.getId());

        System.out.println(requestURL);

        RestTemplate restClient = new RestTemplate();

        restClient.delete(requestURL);
    }


    @Test
    public void createInvite() {
        User user = userEntityFactory.createUserData();
        user= userRepository.save(user);

        assertNotNull(user.getId());

        System.out.println("USER_ID : "+user.getId());

        Invite invite = userEntityFactory.createInviteData(user);
        invite= inviteRepository.save(invite);

        assertNotNull(invite.getId());

        System.out.println("INVITE_ID : "+invite.getId());

        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "CreateInvite");

        System.out.println(requestURL);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<Invite> request = new HttpEntity<>(invite, headers);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restClient = new RestTemplate();

        //HttpEntity<Comment> request = new HttpEntity<>(comment, headers);

        //ResponseEntity<Comment> response = restClient.exchange(requestURL, HttpMethod.GET, request, Comment.class);


        //Comment getComment = response.getBody();



        //getComment= restClient.getForObject(requestURL, Comment.class);


        ResponseEntity<Invite> httpResponse = restClient.postForEntity(requestURL,request, Invite.class);

        assertEquals(httpResponse.getStatusCode(), HttpStatus.CREATED);

        Invite getInvite = httpResponse.getBody();
        assertNotNull(getInvite.getId());
    }

    @Test
    public void createReport() {
        User user = userEntityFactory.createUserData();
        user= userRepository.save(user);

        assertNotNull(user.getId());

        System.out.println("USER_ID : "+user.getId());

        Report report = userEntityFactory.createSelfReport(user);
        report= reportRepository.save(report);

        assertNotNull(report.getId());

        System.out.println("REPORT_ID : "+report.getId());

        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "CreateReport");

        System.out.println(requestURL);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<Report> request = new HttpEntity<>(report, headers);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restClient = new RestTemplate();

        //HttpEntity<Comment> request = new HttpEntity<>(comment, headers);

        //ResponseEntity<Comment> response = restClient.exchange(requestURL, HttpMethod.GET, request, Comment.class);


        //Comment getComment = response.getBody();



        //getComment= restClient.getForObject(requestURL, Comment.class);


        ResponseEntity<Report> httpResponse = restClient.postForEntity(requestURL,request, Report.class);

        assertEquals(httpResponse.getStatusCode(), HttpStatus.CREATED);

        Report getReport = httpResponse.getBody();
        assertNotNull(getReport.getId());
    }

}


