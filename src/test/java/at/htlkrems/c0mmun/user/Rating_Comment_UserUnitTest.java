package at.htlkrems.c0mmun.user;

import at.htlkrems.c0mmun.domain.comment.ICommentRepository;
import at.htlkrems.c0mmun.domain.user.IRating_Comment_UserRepository;
import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.user.Rating_Comment_User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static at.htlkrems.c0mmun.comment.commentEntityFactory.createCommentData;
import static at.htlkrems.c0mmun.user.userEntityFactory.createUserData;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@WebAppConfiguration
@Transactional
public class Rating_Comment_UserUnitTest {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private ICommentRepository commentRepository;

    @Autowired
    private IRating_Comment_UserRepository likes_comment_userRepository;

    @Test
    public void createLikes_Comment_User() {
        Rating_Comment_User rating_comment_user = userEntityFactory.createLikes_Comment_UserData(
                commentRepository.save(createCommentData()),
                userRepository.save(createUserData()));
        Rating_Comment_User savedEntity = likes_comment_userRepository.save(rating_comment_user);

        assertNotNull(savedEntity.getId());
        assertEquals(rating_comment_user.getDate(), savedEntity.getDate());
    }

    @Test
    public void updateLikes_Comment_User(){
         Rating_Comment_User rating_comment_user = userEntityFactory.createLikes_Comment_UserData(
                commentRepository.save(createCommentData()),
                userRepository.save(createUserData()));
        Rating_Comment_User savedEntity = likes_comment_userRepository.save(rating_comment_user);

        savedEntity= userEntityFactory.updateLikes_Comment_UserData(savedEntity);
        Rating_Comment_User savedEntity2 = likes_comment_userRepository.save(savedEntity);

        assertEquals(savedEntity2.getDate(), savedEntity.getDate());
    }

    @Test
    public void deleteLikes_Comment_User() {
        Rating_Comment_User rating_comment_user = userEntityFactory.createLikes_Comment_UserData(
                commentRepository.save(createCommentData()),
                userRepository.save(createUserData()));
        Rating_Comment_User savedEntity = likes_comment_userRepository.save(rating_comment_user);

        assertNotNull(savedEntity.getId());
        assertEquals(rating_comment_user.getDate(), savedEntity.getDate());

        likes_comment_userRepository.delete(savedEntity);

        assertFalse(likes_comment_userRepository.existsById(savedEntity.getId()));
    }
}
