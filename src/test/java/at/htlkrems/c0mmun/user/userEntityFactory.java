package at.htlkrems.c0mmun.user;

import at.htlkrems.c0mmun.model.comment.Comment;
import at.htlkrems.c0mmun.model.post.Post;
import at.htlkrems.c0mmun.model.tag.Tag;
import at.htlkrems.c0mmun.model.user.*;

import java.util.Date;

public class userEntityFactory {

    private static int cnt=0;
    public static User createUserData(){

        User user = new User();
        user.setEmail("email"+cnt+"@gmx.at");
        user.setPassword("password123");
        user.setUsername("username"+cnt);
        user.setRating(12l);

        cnt++;
        return user;
    }

    public static User updateUserData(User savedEntity) {
        savedEntity.setUsername("update");

        return savedEntity;
    }

    public static Invite createInviteData(User user){

        Invite invite = new Invite();
        invite.setEmail("email@gmx.at");
        invite.setUser(user);
        return invite;
    }

    public static Report createSelfReport(User user){

        Report report = new Report();
        report.setDescription("desc");
        report.setReported(user);
        report.setReporter(user);
        return report;
    }


    public static Rating_Comment_User createLikes_Comment_UserData(Comment comment, User user)
    {
        Rating_Comment_User rating_comment_user = new Rating_Comment_User(comment,user);

        rating_comment_user.setDate(new Date());

        return rating_comment_user;
    }

    public static Rating_Comment_User updateLikes_Comment_UserData(Rating_Comment_User rating_comment_user)
    {
        rating_comment_user.setDate(new Date());

        return rating_comment_user;
    }

    public static Rating_Post_User createLikes_Post_UserData(Post post, User user)
    {
        Rating_Post_User rating_post_user = new Rating_Post_User(post,user);

        rating_post_user.setDate(new Date());

        return rating_post_user;
    }

    public static Rating_Post_User updateLikes_Post_UserData(Rating_Post_User rating_post_user)
    {
        rating_post_user.setDate(new Date());

        return rating_post_user;
    }

    public static Rating_Tag_User createRating_Tag_UserData(Tag tag, User user)
    {
        Rating_Tag_User rating_tag_user = new Rating_Tag_User(tag,user);

        rating_tag_user.setDate(new Date());

        return rating_tag_user;
    }

    public static Rating_Tag_User updateRating_Tag_UserData(Rating_Tag_User rating_tag_user)
    {
        rating_tag_user.setDate(new Date());

        return rating_tag_user;
    }

}
