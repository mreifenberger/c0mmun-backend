package at.htlkrems.c0mmun.comment;

import at.htlkrems.c0mmun.domain.comment.ICommentRepository;
import at.htlkrems.c0mmun.model.comment.Comment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@WebAppConfiguration
@Transactional
public class CommentUnitTest {

    @Autowired
    private ICommentRepository commentRepository;

    @Test
    public void createComment() {
        Comment comment = commentEntityFactory.createCommentData();
        Comment savedEntity = commentRepository.save(comment);

        assertNotNull(savedEntity.getId());
        assertEquals(comment.getText(), savedEntity.getText());
    }

    @Test
    public void updateComment(){
        Comment comment = commentEntityFactory.createCommentData();
        Comment savedEntity = commentRepository.save(comment);

        savedEntity= commentEntityFactory.updateCommentData(savedEntity);
        Comment savedEntity2 = commentRepository.save(savedEntity);

        assertEquals(savedEntity2.getText(), savedEntity.getText());
    }

    @Test
    public void deleteComment() {
        Comment comment = commentEntityFactory.createCommentData();
        Comment savedEntity = commentRepository.save(comment);

        assertNotNull(savedEntity.getId());
        assertEquals(comment.getText(), savedEntity.getText());

        commentRepository.delete(savedEntity);

        assertFalse(commentRepository.existsById(savedEntity.getId()));
    }
}
