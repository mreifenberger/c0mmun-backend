package at.htlkrems.c0mmun.comment;

import at.htlkrems.c0mmun.model.comment.Comment;

public class commentEntityFactory {

    public static Comment createCommentData(){

        Comment comment = new Comment();
        comment.setText("comment");

        return comment;
    }

    public static Comment updateCommentData(Comment savedEntity) {
        savedEntity.setText("updated text");

        return savedEntity;
    }
}
