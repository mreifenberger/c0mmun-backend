package at.htlkrems.c0mmun.comment;

import at.htlkrems.c0mmun.domain.comment.ICommentRepository;
import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.comment.Comment;
import at.htlkrems.c0mmun.model.user.Rating_Comment_User;
import at.htlkrems.c0mmun.model.user.User;
import at.htlkrems.c0mmun.user.userEntityFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class CommentIntegrationTest {

    @Autowired
    private ICommentRepository commentRepository;

    @Autowired
    private IUserRepository userRepository;

    private static final String URL_TEMPLATE = "%s/%s/%s";

    private static final String ADVANCED_URL_TEMPLATE = "%s/%s/%s/%s";

    private String serverURL = "http://127.0.0.1:8080";

    private String contextRoot = "Comments";


    @Test
    public void createComment() {


        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "CreateComment");

        System.out.println(requestURL);

        RestTemplate restClient = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));

        Comment comment = commentEntityFactory.createCommentData();

        HttpEntity<Comment> request = new HttpEntity<>(comment, headers);
        ResponseEntity<Comment> response = restClient.postForEntity(requestURL, request, Comment.class); //Anfrage mit Post abschicken (URL, request, erwarteter Returnwert)
        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        Comment createdComment = response.getBody();
        assertNotNull(createdComment.getId());
    }

    @Test
    public void deleteComment() {

        Comment comment = commentEntityFactory.createCommentData();
        comment= commentRepository.save(comment);

        assertNotNull(comment.getId());

        System.out.println("COMMENT_ID : "+comment.getId());

        String requestURL = String.format(ADVANCED_URL_TEMPLATE, serverURL, contextRoot, "DeleteComment",comment.getId());

        System.out.println(requestURL);

        RestTemplate restClient = new RestTemplate();

        restClient.delete(requestURL);
    }


    @Test
    public void getComment() {

        Comment comment = commentEntityFactory.createCommentData();
        comment= commentRepository.save(comment);

        assertNotNull(comment.getId());

        System.out.println("COMMENT_ID : "+comment.getId());

        String requestURL = String.format(ADVANCED_URL_TEMPLATE, serverURL, contextRoot, "Comment",comment.getId());

        System.out.println(requestURL);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restClient = new RestTemplate();

        //HttpEntity<Comment> request = new HttpEntity<>(comment, headers);

        //ResponseEntity<Comment> response = restClient.exchange(requestURL, HttpMethod.GET, request, Comment.class);


        //Comment getComment = response.getBody();



        //getComment= restClient.getForObject(requestURL, Comment.class);


        ResponseEntity<Comment> httpResponse = restClient.getForEntity(requestURL, Comment.class);

        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);

        Comment getComment = httpResponse.getBody();
        assertEquals(getComment.getId(), comment.getId());
    }


    @Test
    public void rateComment() {

        Comment comment = commentEntityFactory.createCommentData();
        comment= commentRepository.save(comment);

        assertNotNull(comment.getId());

        System.out.println("COMMENT_ID : "+comment.getId());

        User user  = userEntityFactory.createUserData();
        user= userRepository.save(user);

        System.out.println("USER_ID : "+user.getId());

        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "RateComment");

        System.out.println(requestURL);

        Rating_Comment_User rating= userEntityFactory.createLikes_Comment_UserData(comment,user);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<Rating_Comment_User> request = new HttpEntity<>(rating, headers);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restClient = new RestTemplate();

        //HttpEntity<Comment> request = new HttpEntity<>(comment, headers);

        //ResponseEntity<Comment> response = restClient.exchange(requestURL, HttpMethod.GET, request, Comment.class);


        //Comment getComment = response.getBody();



        //getComment= restClient.getForObject(requestURL, Comment.class);


        ResponseEntity<Rating_Comment_User> httpResponse = restClient.postForEntity(requestURL,request, Rating_Comment_User.class);

        assertEquals(httpResponse.getStatusCode(), HttpStatus.CREATED);

        Rating_Comment_User getRating_Comment_User = httpResponse.getBody();
        assertNotNull(rating.getId());
    }

}


