package at.htlkrems.c0mmun.post;


import at.htlkrems.c0mmun.CustomPageImpl;
import at.htlkrems.c0mmun.domain.post.ICategoryRepository;
import at.htlkrems.c0mmun.domain.post.IPostRepository;
import at.htlkrems.c0mmun.domain.tag.ITagRepository;
import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.post.Category;
import at.htlkrems.c0mmun.model.post.Post;
import at.htlkrems.c0mmun.model.tag.Tag;
import at.htlkrems.c0mmun.model.user.Rating_Post_User;
import at.htlkrems.c0mmun.model.user.User;
import at.htlkrems.c0mmun.tag.tagEntityFactory;
import at.htlkrems.c0mmun.user.userEntityFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class PostIntegrationTest {

    @Autowired
    private IPostRepository postRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private ITagRepository tagRepository;

    @Autowired
    private ICategoryRepository categoryRepository;

    private static final String URL_TEMPLATE = "%s/%s/%s";

    private static final String ADVANCED_URL_TEMPLATE = "%s/%s/%s/%s";

    private String serverURL = "http://127.0.0.1:8080";

    private String contextRoot = "Posts";


    @Test
    public void createPost() {


        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "CreatePost");

        System.out.println(requestURL);

        RestTemplate restClient = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));

        Post post = postEntityFactory.createPostData();

        HttpEntity<Post> request = new HttpEntity<>(post, headers);
        ResponseEntity<Post> response = restClient.postForEntity(requestURL, request, Post.class); //Anfrage mit Post abschicken (URL, request, erwarteter Returnwert)
        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        Post createdPost = response.getBody();
        assertNotNull(createdPost.getId());
    }

    @Test
    public void deletePost() {

        Post post = postEntityFactory.createPostData();
        post= postRepository.save(post);

        assertNotNull(post.getId());

        System.out.println("POST_ID : "+post.getId());

        String requestURL = String.format(ADVANCED_URL_TEMPLATE, serverURL, contextRoot, "DeletePost",post.getId());

        System.out.println(requestURL);

        RestTemplate restClient = new RestTemplate();

        restClient.delete(requestURL);
    }


    @Test
    public void getPost() {
        Post post = postEntityFactory.createPostData();
        post= postRepository.save(post);

        assertNotNull(post.getId());

        System.out.println("POST_ID : "+post.getId());

        String requestURL = String.format(ADVANCED_URL_TEMPLATE, serverURL, contextRoot, "Post",post.getId());

        System.out.println(requestURL);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restClient = new RestTemplate();

        //HttpEntity<Comment> request = new HttpEntity<>(comment, headers);

        //ResponseEntity<Comment> response = restClient.exchange(requestURL, HttpMethod.GET, request, Comment.class);


        //Comment getComment = response.getBody();



        //getComment= restClient.getForObject(requestURL, Comment.class);


        ResponseEntity<Post> httpResponse = restClient.getForEntity(requestURL, Post.class);

        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);

        Post getPost = httpResponse.getBody();
        assertEquals(getPost.getId(), post.getId());
    }

    @Test
    public void getPostsFromUser() {

        User user  = userEntityFactory.createUserData();
        user= userRepository.save(user);

        System.out.println("USER_ID : "+user.getId());


        Post post = postEntityFactory.createPostData();
        post.setUser(user);

        post= postRepository.save(post);

        assertNotNull(post.getId());

        System.out.println("POST_ID : "+post.getId());

        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "User/"+user.getId()+"?page=0&size=5");

        System.out.println(requestURL);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));


        RestTemplate restClient = new RestTemplate();

        //HttpEntity<Comment> request = new HttpEntity<>(comment, headers);

        //ResponseEntity<Comment> response = restClient.exchange(requestURL, HttpMethod.GET, request, Comment.class);


        //Comment getComment = response.getBody();



        //getComment= restClient.getForObject(requestURL, Comment.class);
        //ParameterizedTypeReference<CustomPageImpl<Post>> ptr =
        //        new ParameterizedTypeReference<CustomPageImpl<Post>> () {
        //        };

        //restClient.exchange(requestURL,HttpMethod.GET, null, ptr);


        ResponseEntity<CustomPageImpl<Post>> response = restClient.exchange(
                requestURL,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<CustomPageImpl<Post>>(){});


        assertEquals(response.getStatusCode(), HttpStatus.OK);

        CustomPageImpl<Post> getPosts = response.getBody();
        System.out.println(getPosts.getTotalElements());
        assertEquals(1,getPosts.getTotalElements());
    }

    @Test
    public void getPostWithTag() {

        User user  = userEntityFactory.createUserData();
        user= userRepository.save(user);

        Tag tag = tagEntityFactory.createTagData();
        tag = tagRepository.save(tag);

        System.out.println("USER_ID : "+user.getId());


        Post post = postEntityFactory.createPostData();
        post.setUser(user);
        post.getTags().add(tag);

        post= postRepository.save(post);

        assertNotNull(post.getId());

        System.out.println("POST_ID : "+post.getId());

        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "Tag/"+tag.getId()+"?page=0&size=5");

        System.out.println(requestURL);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));


        RestTemplate restClient = new RestTemplate();

        ResponseEntity<CustomPageImpl<Post>> response = restClient.exchange(
                requestURL,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<CustomPageImpl<Post>>(){});


        assertEquals(response.getStatusCode(), HttpStatus.OK);

        CustomPageImpl<Post> getPosts = response.getBody();
        System.out.println(getPosts.getTotalElements());
        assertEquals(1,getPosts.getTotalElements());
    }

    @Test
    public void getPostWithCategory() {

        User user  = userEntityFactory.createUserData();
        user= userRepository.save(user);

        Category category = postEntityFactory.createCategoryData();
        category = categoryRepository.save(category);


        Post post = postEntityFactory.createPostData();
        post.setUser(user);
        post.getCategories().add(category);

        post= postRepository.save(post);

        assertNotNull(post.getId());

        System.out.println("POST_ID : "+post.getId());

        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "Category/"+category.getId()+"?page=0&size=5");

        System.out.println(requestURL);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));


        RestTemplate restClient = new RestTemplate();

        ResponseEntity<CustomPageImpl<Post>> response = restClient.exchange(
                requestURL,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<CustomPageImpl<Post>>(){});


        assertEquals(response.getStatusCode(), HttpStatus.OK);

        CustomPageImpl<Post> getPosts = response.getBody();
        System.out.println(getPosts.getTotalElements());
        assertEquals(1,getPosts.getTotalElements());
    }

    @Test
    public void ratePost() {
        Post post = postEntityFactory.createPostData();
        post= postRepository.save(post);

        assertNotNull(post.getId());

        System.out.println("POST_ID : "+post.getId());

        User user  = userEntityFactory.createUserData();
        user= userRepository.save(user);

        System.out.println("USER_ID : "+user.getId());

        String requestURL = String.format(URL_TEMPLATE, serverURL, contextRoot, "RatePost");

        System.out.println(requestURL);

        Rating_Post_User rating= userEntityFactory.createLikes_Post_UserData(post,user);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<Rating_Post_User> request = new HttpEntity<>(rating, headers);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restClient = new RestTemplate();

        //HttpEntity<Comment> request = new HttpEntity<>(comment, headers);

        //ResponseEntity<Comment> response = restClient.exchange(requestURL, HttpMethod.GET, request, Comment.class);


        //Comment getComment = response.getBody();



        //getComment= restClient.getForObject(requestURL, Comment.class);


        ResponseEntity<Rating_Post_User> httpResponse = restClient.postForEntity(requestURL,request, Rating_Post_User.class);

        assertEquals(httpResponse.getStatusCode(), HttpStatus.CREATED);

        Rating_Post_User getRating_Post_User = httpResponse.getBody();
        assertNotNull(rating.getId());
    }

}


