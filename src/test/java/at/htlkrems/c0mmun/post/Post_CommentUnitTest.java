package at.htlkrems.c0mmun.post;

import at.htlkrems.c0mmun.domain.comment.ICommentRepository;
import at.htlkrems.c0mmun.domain.post.IPostRepository;
import at.htlkrems.c0mmun.domain.post.IPost_CommentRepository;
import at.htlkrems.c0mmun.model.post.Post_Comment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static at.htlkrems.c0mmun.comment.commentEntityFactory.createCommentData;
import static at.htlkrems.c0mmun.post.postEntityFactory.createPostData;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@WebAppConfiguration
@Transactional
public class Post_CommentUnitTest {

    @Autowired
    private IPostRepository postRepository;

    @Autowired
    private ICommentRepository commentRepository;

    @Autowired
    private IPost_CommentRepository post_commentRepository;

    @Test
    public void createPost_Comment() {
        Post_Comment post_comment= postEntityFactory.createPost_CommentData(
                postRepository.save(createPostData()),
                commentRepository.save(createCommentData()));
        Post_Comment savedEntity = post_commentRepository.save(post_comment);

        assertNotNull(savedEntity.getId());
        assertEquals(post_comment.getDate(), savedEntity.getDate());
    }

    @Test
    public void updatePost_Comment(){
        Post_Comment post_comment= postEntityFactory.createPost_CommentData(
                postRepository.save(createPostData()),
                commentRepository.save(createCommentData()));
        Post_Comment savedEntity = post_commentRepository.save(post_comment);

        savedEntity= postEntityFactory.updatePost_CommentData(savedEntity);
        Post_Comment savedEntity2 = post_commentRepository.save(savedEntity);

        assertEquals(savedEntity2.getDate(), savedEntity.getDate());
    }

    @Test
    public void deletePost_Comment() {
        Post_Comment post_comment= postEntityFactory.createPost_CommentData(
                postRepository.save(createPostData()),
                commentRepository.save(createCommentData()));
        Post_Comment savedEntity = post_commentRepository.save(post_comment);

        assertNotNull(savedEntity.getId());
        assertEquals(post_comment.getDate(), savedEntity.getDate());

        post_commentRepository.delete(savedEntity);

        assertFalse(post_commentRepository.existsById(savedEntity.getId()));
    }
}
