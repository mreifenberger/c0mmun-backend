package at.htlkrems.c0mmun.post;

import at.htlkrems.c0mmun.model.comment.Comment;
import at.htlkrems.c0mmun.model.post.Category;
import at.htlkrems.c0mmun.model.post.Post;
import at.htlkrems.c0mmun.model.post.Post_Comment;

import java.util.Date;

public class postEntityFactory {

    public static Post createPostData(){

        Post post = new Post();
        post.setName("name");

        return post;
    }

    public static Post updatePostData(Post savedEntity) {
        savedEntity.setName("updated");

        return savedEntity;
    }

    public static Category createCategoryData(){

        Category category = new Category();
        category.setName("name");

        return category;
    }

    public static Category updateCategoryData(Category savedEntity) {
        savedEntity.setName("updated");

        return savedEntity;
    }

    public static Post_Comment createPost_CommentData(Post post, Comment comment)
    {
        Post_Comment post_comment = new Post_Comment(post,comment);

        post_comment.setDate(new Date());

        return post_comment;
    }

    public static Post_Comment updatePost_CommentData(Post_Comment post_comment)
    {
        post_comment.setDate(new Date());

        return post_comment;
    }
}
