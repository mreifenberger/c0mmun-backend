package at.htlkrems.c0mmun.post;

import at.htlkrems.c0mmun.domain.post.ICategoryRepository;
import at.htlkrems.c0mmun.domain.post.IPostRepository;
import at.htlkrems.c0mmun.domain.tag.ITagRepository;
import at.htlkrems.c0mmun.domain.user.IUserRepository;
import at.htlkrems.c0mmun.model.post.Category;
import at.htlkrems.c0mmun.model.post.Post;
import at.htlkrems.c0mmun.model.tag.Tag;
import at.htlkrems.c0mmun.model.user.User;
import at.htlkrems.c0mmun.tag.tagEntityFactory;
import at.htlkrems.c0mmun.user.userEntityFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@WebAppConfiguration
@Transactional
public class PostUnitTest {

    @Autowired
    private IPostRepository postRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private ITagRepository tagRepository;

    @Autowired
    private ICategoryRepository categoryRepository;

    @Test
    public void createPost() {
        Post post= postEntityFactory.createPostData();
        Post savedEntity = postRepository.save(post);

        assertNotNull(savedEntity.getId());
        assertEquals(post.getName(), savedEntity.getName());
    }

    @Test
    public void updatePost(){
        Post post= postEntityFactory.createPostData();
        Post savedEntity = postRepository.save(post);

        savedEntity= postEntityFactory.updatePostData(savedEntity);
        Post savedEntity2 = postRepository.save(savedEntity);

        assertEquals(savedEntity2.getName(), savedEntity.getName());
    }

    @Test
    public void deletePost() {
        Post post= postEntityFactory.createPostData();
        Post savedEntity = postRepository.save(post);

        assertNotNull(savedEntity.getId());
        assertEquals(post.getName(), savedEntity.getName());

        postRepository.delete(savedEntity);

        assertFalse(postRepository.existsById(savedEntity.getId()));
    }

    @Test
    public void PostbyUser() {
        Post post = postEntityFactory.createPostData();
        Post savedEntity = postRepository.save(post);

        User user = userEntityFactory.createUserData();
        user = userRepository.save(user);

        post.setUser(user);
        postRepository.save(post);

        Page<Post> posts= postRepository.FindPostsByUser(user.getId(),new PageRequest(0,10));

        assertEquals(1, posts.getTotalElements());


    }

    @Test
    @Transactional
    public void PostWithTag() {
        Post post = postEntityFactory.createPostData();
        Post savedEntity = postRepository.save(post);

        Tag tag = tagEntityFactory.createTagData();
        tag = tagRepository.save(tag);

        post.getTags().add(tag);
        postRepository.save(post);

        Page<Post> posts= postRepository.FindPostsByTag(tag.getId(),PageRequest.of(0,10));

        assertEquals(1, posts.getTotalElements());

    }

    @Test
    public void PostWithCategory() {
        Post post = postEntityFactory.createPostData();
        Post savedEntity = postRepository.save(post);

        Category cat = postEntityFactory.createCategoryData();
        cat = categoryRepository.save(cat);

        post.getCategories().add(cat);
        postRepository.save(post);



        Page<Post> posts= postRepository.FindPostsByCategory(cat.getId(),PageRequest.of(0,10));

        assertEquals(1, posts.getTotalElements());


    }

}
